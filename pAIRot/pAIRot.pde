import codeanticode.gsvideo.*;
import jp.nyatla.nyar4psg.*; 
import processing.opengl.*;
import saito.objloader.*;

String camPara = "camera_para.dat";

GSCapture cam;
MultiMarker nya;

float pPosX = 0, pPosY = 0, pPosZ = 0, pRotY = 0, pRotZ = 0, pScale = 1, pRotX =  0;
PVector pAbs = new PVector(0,0,0), sAbs = new PVector(0,0,0), oAbs = new PVector(0,0,0);
Parrot pUser;
Star star;
Wing lwing;
Wing rwing;
Obstacle obstacle;
long time;
float flapdx = 0.1;
int starInvisible = 0;
int collisionType = 0;
boolean showBB = false;
boolean isPaused = true;

int energy = 100;
float constSpeed = 15;
float mxSpeed = 60;
float mxSpeedY = 5; 
float speedadd = 10;
float speedYadd = 10;
float speeddim = 0.5;
float speedYdim = 5;
float rotYdx = 0.1;
int starInvMin = 5, starInvMax = 10;
int energyPerStar = 10;     // add on hit
int energyPerObstacle = 1;  // subtract on hit
float broadsweepRange = 60;
float floorLv = -30;

void setup() {
  size(640,480,OPENGL);
  colorMode(RGB, 100);
  frameRate(60);
  // init camera
  // "Lenovo EasyCamera"
  // "HP Basic Starter Camera"
  cam=new GSCapture(this,640,480, "HP Basic Starter Camera", 30);
  cam.start();
  
  // init nyar
  nya=new MultiMarker(this,width,height,"camera_para.dat",NyAR4PsgConfig.CONFIG_PSG);
  nya.addARMarker("4x4_1.patt",80);//id=0
  nya.addARMarker("patt.kanji",80);//id=1
  nya.addARMarker("patt.hiro",80);//id=2
  
  // init models
  pUser = new Parrot(this, "parrot_body.obj");
  lwing = new Wing(this, "parrot_lwing.obj");
  rwing = new Wing(this, "parrot_rwing.obj");
  star = new Star(this, "star.obj");
  star.setPosY(-50);
  obstacle = new Obstacle(this, "obstacle.obj");
  obstacle.setPosY(-50);
  
  time = System.nanoTime(); 
}

void draw(){
  if(isPaused) {
    background(10);
    textSize(20);
    fill(255, 255, 255);
    text("PAUSED", width/2-35, 50);
    textSize(15);
    text("LEFT/RIGHT - turn around", width/2-120, 100);
    text("UP/DOWN - accelerate and decelerate", width/2-180, 120);
    text("CTRL/SPACEBAR - fly up and down", width/2-165, 140);
    text("R - reset game", width/2-80, 160);
    text("P - pause and unpause", width/2-110, 180);
    
    
  } else {
    if(frameCount%20 == 0)
      energy--;
    if (starInvisible > 0 && frameCount % 60 == 0)
      starInvisible--;
     
     
    if (cam.available() !=true)
        return;
    
    cam.read();
    nya.detect(cam);
    background(0);
    lights();
    nya.drawBackground(cam);
    noFill();
    
    // display end screen HUD
    if (energy <= 0){ 
      textSize(20);
      fill(0, 102, 153);
      text("0 energy", width-120, 25);
      fill(200, 0, 50);
      text("GAME OVER", width/2-50, height/2);
      return;
    } 
    
    // check collisions
    collisionType = checkCollisions();
    if (collisionType%2 != 0) { // last bit set (star collision)     
      energy += energyPerStar;
      starInvisible = (int)random(starInvMin,starInvMax);
    }
    
    // display parrot
    if(nya.isExistMarker(0)){
      nya.beginTransform(0);
      
      pushMatrix();
      updateParrot(collisionType);
      popMatrix();
      // get global screen coords for parrot
      PMatrix3D transf = nya.getMarkerMatrix(0);
      pAbs = getAbsCoords(transf, pUser.getPosition(), pUser.getRotation());
      pushMatrix();
      lwing.render3D();
      popMatrix();
      pushMatrix();
      rwing.render3D();
      popMatrix();
  
      nya.endTransform();
    } 
    
    // display star
    if(nya.isExistMarker(1) && starInvisible <= 0){
      nya.beginTransform(1);
      pushMatrix();
      star.render3D();
      popMatrix();
      // get global screen coords for star
      PMatrix3D transf = nya.getMarkerMatrix(1);
      sAbs = getAbsCoords(transf, star.getPosition(), star.getRotation());
      nya.endTransform();
    }
    
    // display obstacle
   if(nya.isExistMarker(2)){
      nya.beginTransform(2);
      pushMatrix();
      obstacle.render3D();
      popMatrix();
      // get global screen coords for obstacle
      PMatrix3D transf = nya.getMarkerMatrix(2);
      oAbs = getAbsCoords(transf, obstacle.getPosition(), obstacle.getRotation());
      nya.endTransform();
    } 
    
    // display energy HUD
    fill(0, 102, 153);
    textSize(20);
    text(energy + " energy", width-120, 25);
    
    // debug
    if(showBB){
      stroke(1);
      noFill();
      pushMatrix();
      translate(pAbs.x, pAbs.y, pAbs.z);
      sphere(broadsweepRange/2);
      popMatrix();
      pushMatrix();
      translate(sAbs.x, sAbs.y, sAbs.z);
      sphere(broadsweepRange/2);
      popMatrix();
      pushMatrix();
      translate(oAbs.x, oAbs.y, oAbs.z);
      sphere(broadsweepRange/2);
      popMatrix();
      noStroke();
    }
  }
}        

protected void updateParrot(int collisionType){
  
  if (collisionType > 1) // hit obstacle
    pUser.speed = 0;
  
  // for equalizing movement speed on slower/faster computers
  long oldtime = time;
  time = System.nanoTime();
  float factor = (time - oldtime)/10000000;
  
  // calc new position
  float move = (float) ((float)pUser.speed*0.04)*factor;
  float dX = move * (float)Math.cos(pRotY);
  float dZ = move * (float)Math.sin(pRotY);
  pPosX += dX;
  pPosZ -= dZ;
  
  if (collisionType > 1){ // hit obstacle
    pPosX -= 1;
    pPosZ += 1; 
  }
  
  // collision with floor
  if (pPosY > floorLv) {
    pUser.speedY = 0;
    pPosY = floorLv;
  } else {
    pPosY += ((float)pUser.speedY*0.04)*factor;
  }
  
  pUser.setPosition(pPosX, pPosY, pPosZ);
  pUser.setRotation(pRotX, pRotY, 0);
  pUser.render3D();
  
  // smooth speed reduction (forward/backward)
  float ds = 0;
  if (pUser.speed < constSpeed)
    ds = speeddim;
  else if (pUser.speed > constSpeed)
    ds = -speeddim;
    
  pUser.speed += ds;
  
  // smooth speed reduction (up/down)
  ds = 0;
  if (pUser.speedY < 0)
    ds = speedYdim;
  else if (pUser.speedY > 0)
    ds = -speedYdim;
    
  pUser.speedY += ds;
  
  // wing flapping
  if (lwing.getRotX() < -0.3)
    flapdx = 0.02*factor;
  else if (lwing.getRotX() > 0.3)
    flapdx = -0.02*factor;

  lwing.setPosition(pPosX, pPosY, pPosZ);
  lwing.setRotation(lwing.getRotX()+flapdx, pRotY, pRotZ);
  rwing.setPosition(pPosX, pPosY, pPosZ);
  rwing.setRotation(rwing.getRotX()-flapdx, pRotY, pRotZ);
  
}


void keyPressed() {
  if (key == CODED && keyCode == LEFT)      // turn left
    pRotY += rotYdx;
  
  if (key == CODED && keyCode == RIGHT)     // turn right
    pRotY -= rotYdx;
  
  if (key == CODED && keyCode == UP)       // add speed
    if (pUser.speed < mxSpeed)
      pUser.speed += speedadd;
  
  if (key == CODED && keyCode == DOWN)     // reduce speed
    if (pUser.speed > -mxSpeed)
      pUser.speed -= speedadd;
  
  if (key == CODED && keyCode == CONTROL)  // down
    if (pUser.speedY > -mxSpeedY)
      pUser.speedY += speedYadd;
  
  if (key == ' '){                         // up 
    if (pUser.speedY < mxSpeedY) 
      pUser.speedY -= speedYadd;
  }
  
  if (key == 'R' || key == 'r'){           // reset
    energy = 100;
    pPosX = 0;
    pPosY = 0;
    pPosZ = 0;
    pRotY = 0;
    pRotZ = 0;
    pScale = 1;
    pRotX =  0;
    pUser.speed = 0; 
    starInvisible = 0;
    collisionType = 0;
    time = System.nanoTime();
  }
  
  if (key == 'P' || key == 'p'){
    isPaused = !isPaused;
  }
 }
 
 public PVector getAbsCoords(PMatrix3D t, PVector p, PVector r) {
   pushMatrix();
   resetMatrix();
   applyMatrix(t);
   translate(p.x,p.z,-p.y);
   PVector abs = new PVector(screenX(0,0,0), screenY(0,0,0), screenZ(0,0,0)) ;
   popMatrix();
   return abs;
 }
 
 // 00 - no collision, 01 - star collision, 11 - obstacle & star collision, 10 - obstacle collision (bitwise)
 int checkCollisions() {
   int collisionType = 0;
   
   // broadphase
  if (starInvisible <= 0 && sqrt(pow((pAbs.x-sAbs.x),2)+pow((pAbs.y-sAbs.y),2)+pow((pAbs.z-sAbs.z),2)) < broadsweepRange)
    collisionType = collisionType | 1; // set last bit 01
  if (sqrt(pow((pAbs.x-oAbs.x),2)+pow((pAbs.y-oAbs.y),2)+pow((pAbs.z-oAbs.z),2)) < broadsweepRange){
    collisionType = collisionType | 2; // set second-to-last bit 10
    energy -= energyPerObstacle;
  }
    
  return collisionType;
 }
