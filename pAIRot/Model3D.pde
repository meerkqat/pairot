public abstract class Model3D{
  protected float pos_x = 0, pos_y = 0, pos_z = 0; 	//X, Y, Z position coordinates	 
  protected float rot_x = 0, rot_y = 0, rot_z = 0;	//X, Y, Z rotation
  protected float sca_x = 1, sca_y = 1, sca_z = 1;	//X, Y, Z scale of object
  protected OBJModel m_Obj;
  
  public void setPosition(float p_X, float p_Y, float p_Z){
    pos_x = p_X;
    pos_y = p_Y; 
    pos_z = p_Z;
  }
  
  public void setRotation(float p_X, float p_Y, float p_Z){
    rot_x = p_X;
    rot_y = p_Y; 
    rot_z = p_Z;
    
  }
  
  public void setScaling(float p_X, float p_Y, float p_Z){
    sca_x = p_X;
    sca_y = p_Y;
    sca_z = p_Z;
  }
  
  public PVector getPosition() {
    return new PVector(pos_x, pos_y, pos_z);
  }
  
  public PVector getRotation() {
    return new PVector(rot_x, rot_y, rot_z);
  }
  
  public void setPosY(float y) {
    pos_y = y;
  }
  
  public float getRotX() {
    return rot_x;
  }
  
  public float getPosX() {
    return pos_x;
  }
  
  public float getPosY() {
    return pos_y;
  }
  
  public float getPosZ() {
    return pos_z;
  } 
  public void render3D() {
    rotateX(6*PI/4);
    translate(pos_x, pos_y, pos_z);
    rotateY(rot_y);
    rotateX(rot_x);
    rotateZ(rot_z);
    //scale(sca_x, sca_y, sca_z);
  }
}
