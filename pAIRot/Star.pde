public class Star extends Model3D {
  OBJModel m_Obj = null;
  
  public Star(PApplet applet, String objPath){
    float s = 40;
    m_Obj = new OBJModel(applet, objPath, "relative", TRIANGLES);
    m_Obj.scale(s,s,s);
    m_Obj.enableTexture();
    m_Obj.enableMaterial();
    noStroke();
  }
  
  public void render3D() {
    rot_y += 0.5/(2*PI);
    
    super.render3D();
    m_Obj.draw();
  }
}
