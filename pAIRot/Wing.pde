public class Wing extends Model3D{
  
  public Wing(PApplet applet, String objPath) {
    float s = 10;
    m_Obj = new OBJModel(applet, objPath, "relative", TRIANGLES);
    m_Obj.scale(s,s,s);
    m_Obj.enableTexture();
    m_Obj.enableMaterial();
    noStroke();
  }
  
  public void render3D() {
    super.render3D();
    m_Obj.draw();
  }
}
