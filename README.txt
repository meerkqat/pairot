pAIRot is an augmented reality game where you fly a parrot around the room, 
collect stars, and avoid obstacles.
Written for a school project in collaboration with Mark Hočevar.

Requires Processing 2.x (written and tested in Processing 2.0.3)
How to run:
	- move the pAIRot directory into Processing's workspace ("My 
	  Documents\Processing" on Windows)
	- open pAIRot.pde
	- on the line where it says
		cam=new GSCapture(this,640,480, "HP Basic Starter Camera", 30);
	  in the setup() function, replace "HP Basic Starter Camera" for the 
	  name of the camera you're using.
	- run pAIRot.pde